import socket
from _thread import *

from config import config
from tools import load_round
from tools import cards
from tools.load_round import *
from tools.player import Player
from tools.process_round import *

list_of_gamers = []


def remove(conn):
    if conn in list_of_gamers:
        list_of_gamers.remove(conn)


def broadcast(message, conn):
    for gamer in list_of_gamers:
        if gamer != conn:
            try:
                gamer.send(message)
            except:
                gamer.close()
                remove(gamer)


def player_thread(conn, addr):
    conn.send("new player connected".encode())
    while True:
        try:
            message = conn.recv(1024)
            if message:
                print("<{0}>: {1}".format(addr[0], message))
                msg_to_send = "<" + addr[0] + ">: " + message
                broadcast(msg_to_send.encode(), conn)
            else:
                remove(conn)
        except:
            continue


if __name__ == '__main__':

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    player1 = Player("john", False)
    player2 = Player("jack", False)
    player3 = Player("luck", True)
    player4 = Player("phil", False)

    assignation = generate_random_cards_for_players(4)
    print("test assignation", assignation)

    send_cards_to_round_players(assignation, player1, player2, player3, player4)

    play_a_round_v2(player1, player2, player3, player4)


    # player1.print()
    # player2.print()
    # player3.print()
    # player4.print()
    # print("hello world", load_round.load_default(1, 5))

    # print("new hello world", cards
    #       .Cards(config.CARDS[10][0], config.CARDS[10][1])
    #       .card_get_int_value())

    # print("new compare world", cards
    #       .Cards(config.CARDS[10][0], config.CARDS[10][1])
    #       .card_compare(cards.Cards(config.CARDS[25][0], config.CARDS[25][1])))

    # server.bind(('0.0.0.0', 8080))
    # server.listen(60)

    # while True:
    #    connection, address = server.accept()
    #    list_of_gamers.append(connection)
    #    print("{0} is connected ".format(address[0]))
    #    start_new_thread(player_thread, (connection, address))

    # connection.close()
    # server.close()
