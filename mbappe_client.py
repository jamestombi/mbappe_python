import select
import socket
import sys

if __name__ == '__main__':
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect to a default address
    server.connect(('127.0.0.1', 8080))
    while True:
        sockets_list = [sys.stdin, server]
        # sockets_list = [server]
        try:
            read_sockets, _, _ = select.select(sockets_list, [], [], 0)
            for sock in read_sockets:
                if sock == server:
                    message = server.recv(1024)
                    print("message is {0}".format(message.decode()))
                else:
                    message = sys.stdin.readline()
                    server.send(message.encode())
                    sys.stdout.write("<ICH>")
                    sys.stdout.write(message)
                    sys.stdout.flush()
        except KeyboardInterrupt:
            server.close()
