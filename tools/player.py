from config.config import *
from tools.cards import *


class Player:
    def __init__(self, id_player, auto_player):
        self.id_player = id_player
        self.auto_player = auto_player
        self.card_played = [0, 0, 0, 0, 0]
        self.cards = []

    def add_card(self, card):
        if self.cards.__len__() < 5:
            self.cards.append(card)

    def check_triple_seven(self):
        total_seven = 0
        for i in range(0, 5):
            if self.cards[i].card_get_int_value() == 7:
                total_seven += 1
        if total_seven >= 3:
            return 1
        return 0

    def check_under_21(self):
        under_21 = 0
        for i in range(0, 5):
            under_21 += self.cards[i].card_get_int_value()
        if under_21 <= 21:
            return 2
        return 0

    def check_a_square(self):
        for i in range(3, 11):
            a_square = 0
            for t in range(0, 5):
                if self.cards[t].card_get_int_value() % i == 0 and self.cards[t].card_get_int_value() / i == 1:
                    a_square += 1
            if a_square == 4:
                return 4

        return 0

    def has_put_a_good_card(self, leader_card, my_card):
        """
        Method used to check if the player puts a card on the table according to what was already played
        The player has the obligation to put a card of the same color of the card of the winner of the previous
        round if he has one
        :param leader_card: the card played by the winner of the previous run
        :param my_card: the card the player is going to play
        :return: True if the card played meets the requirements, False otherwise
        """
        if int(leader_card/8) == int(my_card/8):
            return True
        for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
            if self.card_played[i] != 1:
                if int(leader_card/8) == int(self.cards[i]/8):
                    return False
        return True

    def print(self):
        print("Player id : {0}".format(self.id_player))
        print(">>> cards (", ['[' + CARDS[self.cards[i]][1] + ']'
                              for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER) if self.card_played[i] == 0], ")")

    def play_a_card(self, cards_played_in_run, winner_of_the_run, rank):
        """
        Method called by the player to play a correct card according to who is the running winner and
        the rank of the player in the pool
        :param cards_played_in_run: the array containing information relative to cards played in the run
        :param winner_of_the_run: position of the winner of the previous run, he is the one who starts with rank 0
        :param rank: your rank in the table of play
        :return: The array of cards played completed
        """
        nb_players = cards_played_in_run.__len__()
        position_holder = (rank + winner_of_the_run) % nb_players
        if not self.auto_player:
            correct_card = 4
            while True:
                self.print()
                cur_card_played = input("enter a card : ")
                for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
                    correct_card = 4
                    # Check that the card is a correct card
                    if cur_card_played == CARDS[self.cards[i]][1]:
                        # Check that the card is not played yet
                        if self.card_played[i] == 0:
                            # Check that the card played meets the requirements
                            # if the current player don't have the lead
                            if rank != 0:
                                good_card = self.has_put_a_good_card(cards_played_in_run[winner_of_the_run % nb_players],
                                                                     self.cards[i])
                                if not good_card:
                                    correct_card = 3
                                    break

                            # Fill the array of cards played
                            cards_played_in_run[position_holder] = self.cards[i]
                            self.card_played[i] = 1
                            correct_card = 1
                        else:
                            correct_card = 2
                        break

                if correct_card == 4:
                    print("bad, real card please")
                elif correct_card == 3:
                    print("bad, meet the color please")
                elif correct_card == 2:
                    print("bad, already played")
                elif correct_card == 1:
                    print("good")
                    break
        else:
            if rank != 0:
                for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
                    good_card = self.has_put_a_good_card(cards_played_in_run[winner_of_the_run % nb_players],
                                                         self.cards[i])
                    if good_card and self.card_played[i] == 0:
                        # Fill the array of cards played
                        self.print()
                        cards_played_in_run[position_holder] = self.cards[i]
                        self.card_played[i] = 1
                        break
            else:
                for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
                    if self.card_played[i] == 0:
                        # Fill the array of cards played
                        self.print()
                        cards_played_in_run[position_holder] = self.cards[i]
                        self.card_played[i] = 1
                        break

        return cards_played_in_run


