from config.config import *


def compare_hands_in_a_run(nb_players, cards_of_the_run, winning_player):
    winner_of_the_run = winning_player % nb_players
    # we evaluate the hand of the player who get the lead after this run
    for i in range(0, nb_players):
        # if the card was "Heart", and another player puts a "Heart" card in the table stronger
        # He takes the lead
        # We evaluate the same way the cards of the other players and the one with the strongest card of the
        # same color will win

        # HEART
        if 0 <= cards_of_the_run[winner_of_the_run] <= 7:
            if cards_of_the_run[winner_of_the_run] < cards_of_the_run[i] <= 7:
                winner_of_the_run = i

        # SPADE
        if 8 <= cards_of_the_run[winner_of_the_run] <= 15:
            if cards_of_the_run[winner_of_the_run] < cards_of_the_run[i] <= 15:
                winner_of_the_run = i

        # DIAMOND
        if 16 <= cards_of_the_run[winner_of_the_run] <= 23:
            if cards_of_the_run[winner_of_the_run] < cards_of_the_run[i] <= 23:
                winner_of_the_run = i

        # CLUB
        if 24 <= cards_of_the_run[winner_of_the_run] <= 31:
            if cards_of_the_run[winner_of_the_run] < cards_of_the_run[i] <= 31:
                winner_of_the_run = i

    return winner_of_the_run


def play_a_round(*players):
    nb_players = players.__len__()
    winner_of_the_run = 0
    correct_card = 3
    cards_played_on_the_run = [-1] * nb_players
    for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
        for j in range(0, nb_players):
            # When we have a winner of a run, he plays first.
            # Then it is the player following him in the
            # list of players who plays
            # We do a circular loop to make every player gives their card
            cur_player_of_run = (winner_of_the_run + j) % nb_players
            while True:
                players[cur_player_of_run].print()
                cur_card_played = input("give a card : ")
                for k in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
                    correct_card = 3
                    # The card played is a real card of the game
                    if cur_card_played == CARDS[players[cur_player_of_run].cards[k]][1]:
                        # The card is not yet played by the player
                        if players[cur_player_of_run].card_played[k] == 0:
                            # The player is not the one with the lead
                            if j != 0:
                                print("leader put card {0} follower put {1}"
                                      .format(CARDS[cards_played_on_the_run[winner_of_the_run % nb_players]][1],
                                              CARDS[players[cur_player_of_run].cards[k]][1]))
                                good_card = \
                                    players[cur_player_of_run] \
                                        .has_put_a_good_card(cards_played_on_the_run[winner_of_the_run % nb_players],
                                                             players[cur_player_of_run].cards[k])
                                if not good_card:
                                    print("You have to put a card of the same color, you have one")
                                    correct_card = 3
                                    break

                            # We have a new card in the set
                            cards_played_on_the_run[cur_player_of_run] = players[cur_player_of_run].cards[k]
                            players[cur_player_of_run].card_played[k] = 1
                            correct_card = 2
                        else:
                            correct_card = 1
                        break

                if correct_card == 2:
                    print("card available")
                    break
                elif correct_card == 1:
                    print("card already played")
                elif correct_card == 3:
                    print("give me a correct card (same color or one in your deck)")

        # Just a printing phase to show what happened
        print("current run ", [CARDS[cards_played_on_the_run[i]][1]
                               for i in range(0, cards_played_on_the_run.__len__())])

        # Call the algorithm that gives the winner of the round
        winner_of_the_run = compare_hands_in_a_run(nb_players, cards_played_on_the_run, winner_of_the_run)
        print("winner of the run {0}".format(winner_of_the_run))

    print("winner {0}".format(winner_of_the_run))
    # Hot win situation
    # A player who wins the last run of the round with the lowest card value of a color (3C, 3P, 3K, 3T)
    if cards_played_on_the_run[winner_of_the_run] % 8 == 0:
        print("by HOT WIN")

    return


def play_a_round_v2(*players):
    nb_players = players.__len__()
    winner_of_the_run = 0
    cards_played_on_the_run = [-1] * nb_players
    for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
        for j in range(0, nb_players):
            # When we have a winner of a run, he plays first.
            # Then it is the player following him in the
            # list of players who plays
            # We do a circular loop to make every player gives their card
            cur_player_of_run = (winner_of_the_run + j) % nb_players
            cards_played_on_the_run = players[cur_player_of_run] \
                .play_a_card(cards_played_on_the_run, winner_of_the_run, j)

        # Just a printing phase to show what happened
        print("current run ", [CARDS[cards_played_on_the_run[i]][1]
                               for i in range(0, cards_played_on_the_run.__len__())])

        # Call the algorithm that gives the winner of the round
        winner_of_the_run = compare_hands_in_a_run(nb_players, cards_played_on_the_run, winner_of_the_run)
        print("winner of the run {0}".format(players[winner_of_the_run].id_player))

    print("winner {0}".format(players[winner_of_the_run].id_player))
    # Hot win situation
    # A player who wins the last run of the round with the lowest card value of a color (3C, 3P, 3K, 3T)
    if cards_played_on_the_run[winner_of_the_run] % 8 == 0:
        print("by HOT WIN")
