# from time import time_ns
from time import *
from time import clock_gettime_ns
from random import randint

from config.config import TOTAL_CARDS
from config.config import MAX_CARD_DISTRIBUTION_PER_PLAYER


def generate_random_cards_for_players(nb_players):
    """
    generate a random list of numbers representing the cards in the set of 32 that
    will be used for the round of play
    :param nb_players: the number of players in the round
    :return: the list of number that will be used as cards
    """

    cards_present = ['O'] * TOTAL_CARDS
    current_round_cards_selected = [0] * nb_players * MAX_CARD_DISTRIBUTION_PER_PLAYER

    for j in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
        for i in range(0, nb_players):
            # For UNIX
            cur_time = clock_gettime_ns(CLOCK_MONOTONIC)
            # For Windows
            #cur_time = time_ns()
            # Find a card not given yet in the round and assign it
            while True:
                card = randint(0, cur_time) % TOTAL_CARDS
                if cards_present[card] != 'N':
                    break
            # A not yet assigned card is found, set it assigned and save it
            cards_present[card] = 'N'
            current_round_cards_selected[j + i * MAX_CARD_DISTRIBUTION_PER_PLAYER] = card

    return current_round_cards_selected


def load_default(a, b):
    return a + b


def send_cards_to_round_players(cards_selected, *players):
    """
    Function used to send to different users given in parameters, their set of cards for the round
    :param cards_selected: the array containing cards randomly selected
    :param players: list of players
    :return:
    """
    nb_players = players.__len__()
    for j in range(0, nb_players):
        for i in range(0, MAX_CARD_DISTRIBUTION_PER_PLAYER):
            players[j].cards.append(cards_selected[j * MAX_CARD_DISTRIBUTION_PER_PLAYER + i])


