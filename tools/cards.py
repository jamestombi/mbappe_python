
class Cards:

    def __init__(self, rank_value, name):
        self.rank_value = rank_value
        self.name = name

    # Helps to retrieve the card value (7K has value 7, 10C has value 10)
    def card_get_int_value(self):
        return self.rank_value % 8 + 3

    # Helps comparing two cards
    def card_compare(self, card_b):
        if self.card_get_int_value() < card_b.card_get_int_value():
            return 1
        elif self.card_get_int_value() > card_b.card_get_int_value():
            return -1
        return 0

    # Helps changing cards position
    def card_swap(self, card_b):
        return


